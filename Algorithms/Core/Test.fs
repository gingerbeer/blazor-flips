namespace Algorithms.Core

open System
open FSharp.Data
open MathNet.Numerics
open MathNet.Numerics.Statistics
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open Flips
open Flips.Types
open Flips.SliceMap
open Flips.UnitsOfMeasure
open Flips.Builders


type Model = Model of Objective<1> * SolveResult

type ModelResult =
    { truth: Map<string, float>
      model: Model }

module Test =
    let simple N lambda tau =
        let seed = 123
        let rng = Random.MersenneTwister(seed)
        let gain = 100.0

        let A = CreateMatrix.Random<float>(500, N, seed)

        let x =
            gain * CreateVector.Random<float>(N, seed)
            |> Vector.map (fun x ->
                match rng.NextDouble() < 0.3 with
                | true -> 0.0
                | false -> x)

        let b = A * (x + CreateVector.Random<float>(N, seed)) // Add noise to x

        let truth = b |> Seq.indexed |> Seq.map (fun (i, x) -> $"x{i}", x) |> Map.ofSeq

        try
            let model = BasisPursuit.basisPursuit lambda tau A b |> Model
            { truth = truth; model = model } |> Ok
        with ex ->
            Error $"Failed to compute solution - {ex}"

    let displayResult modelResult =
        match modelResult with
        | Ok({truth = truth; model = model}) ->
            let (Model (objective, result)) = model
            match result with
            | Optimal solution ->
                seq {
                    for (decision, value) in solution.DecisionResults |> Map.toSeq do
                        let (DecisionName name) = decision.Name
                        name, (truth.[name], value) // encapsulate truth and model together
                }
                |> Map.ofSeq
                |> Ok
            | _ -> Error $"Unable to solve. Error: %A{result}"
        | Error e -> Error $"Computing solution failed - {e}"
