namespace Algorithms.Core

open System
open MathNet.Numerics.LinearAlgebra

open MathNet.Numerics.Statistics
open MathNet.Numerics.LinearAlgebra.Double

open Flips
open Flips.Types
open Flips.SliceMap
open Flips.UnitsOfMeasure

type RealType = double

module BasisPursuit =

    (* 
        A Basis Pursuit algorithm based on the notes at:
        https://docs.mosek.com/modeling-cookbook/linear.html Example 2.1

        Solves the problem:

        minimize 1'*(t) s.t. Ax - b < +lambda, -lambda < Ax - b, -t <= x <= +t

        Where A has applied Tikhonov Regularization - (A + tau * I)

        This algorithm is equivalent to LASSO with denoising
        and rank degeneracy mitigation (regularisation).
     *)

    let MAX_VALUE = 1_000_000.0

    let basisPursuit (lambda: RealType) (tau: RealType) (A: Matrix<RealType>) (b: Vector<RealType>) =

        if lambda < 0.0 then
            failwith "Lambda must be positive"

        if tau < 0.0 then
            failwith "Tau must be positive"

        let I = CreateMatrix.DenseIdentity(A.RowCount, A.ColumnCount)
        let A' = A + tau * I // Add Tikhonov regularisation to A

        let A' = seq { for row in A'.ToRowArrays() -> row |> Seq.ofArray }
        let b' = seq { for ele in b -> ele }

        let states =
            seq { for i in 0..(A.ColumnCount-1) -> $"x{i}" }

        if (A.RowCount <> b.Count) then
            failwith $"Column length of A must match length of b - {A.RowCount} != {b.Count}"

        let xDecision =
            DecisionBuilder<1> "x" { for each in states -> Continuous(-MAX_VALUE, +MAX_VALUE) }
            |> SMap.ofSeq

        let zDecision =
            DecisionBuilder<1> "z" { for each in states -> Continuous(-MAX_VALUE, +MAX_VALUE) }
            |> SMap.ofSeq

        let zConstraintUpper =
            ConstraintBuilder "zConstraintUpper" {
                let z = (1.0 * zDecision)
                for each in states -> xDecision.[each] <== z.[each]
            }

        let zConstraintLower =
            ConstraintBuilder "zConstraintLower" {
                let z = (-1.0 * zDecision)
                for each in states -> z.[each] <== xDecision.[each]
            }

        let problemConstraints =
            let lhsExpr = Seq.zip states A' |> SMap.ofSeq
            let rhsExpr = Seq.zip states b' |> SMap.ofSeq

            let problemConstraintUpper =
                ConstraintBuilder "EqualityContraintUpper" {

                    for each in states ->
                        let dotRow =
                            Seq.zip lhsExpr.[each] xDecision
                            |> Seq.map (fun (a, x) -> a * x.Value)
                            |> Seq.sum

                        dotRow - rhsExpr.[each] <== +lambda
                }

            let problemConstraintLower =
                ConstraintBuilder "EqualityContraintLower" {
                    for each in states ->
                        let dotRow =
                            Seq.zip lhsExpr.[each] xDecision
                            |> Seq.map (fun (a, x) -> a * x.Value)
                            |> Seq.sum

                        -lambda <== dotRow - rhsExpr.[each]
                }

            seq {
                yield! problemConstraintUpper
                yield! problemConstraintLower
            }

        let objectiveExpr = zDecision |> sum
        let objective = Objective.create "Minimise-Z" Minimize objectiveExpr

        let model =
            Model.create objective
            |> Model.addConstraints problemConstraints
            |> Model.addConstraints zConstraintUpper
            |> Model.addConstraints zConstraintLower

        objective, Solver.solve { Settings.basic with SolverType = GLOP } model
