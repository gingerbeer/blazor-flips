# Blazor-Flips

This repository is an experiment to get flips running in a blazor webassembly application.

This application requires a dotnet 7.0 SDK installation present on the system path.
The application will also require a modern cmake installation to build the dependencies.

```bash
dotnet --version
```
7.0.102

```bash
cmake --version
```
cmake version 3.25.2

The basic application is a blazor webassembly empty template provided as part of the SDK.
Such a template can be created by:

```bash
dotnet new blazorwasm-empty -ho -o MyCoolApp
```

Modifications have been made to flips to support the Highs solver through Optano. The Highs solver codebase is included as a source dependency and is compiled using emscripten to a WASM target.

The method to build the Highs library can be done by running:

```bash
./build-deps.sh
```
from the project root. This will setup the emcripten SDK (emsdk) and then run emcmake over Highs. Then script builds the Highs library using the systems normal "make" and the emcc/em++ compilers provided by Emscripten. Other build generators (such as Ninja can be used. Also note this process can be replicated easily on Windows using mingw make).

Further instruction on how to setup emscripten with CMake on Windows can be found at:
https://badlydrawnrod.github.io/posts/2020/05/19/emcmake-with-emscripten/

Finally the native wasm built Highs library is referenced in the Client Blazor project:

```xml
    <!-- <NativeFileReference Include="../HiGHS/install/lib/libhighs.a"/> -->
    <!-- <NativeFileReference Include="../HiGHS/install/lib/libhighs.wasm"/> -->
    <NativeFileReference Include="../HiGHS/install/lib/libhighs.bc"/>
```
Experimentation with the type of native file dependency produced has been informative.
The bitcode format seems to show during link stage the libraries main API functions are 
undefined which may indicate the library not being built correctly.

```
...
EXEC : warning : undefined symbol: Highs_addCol (referenced by top-level compiled C/C++ code)
...
```

The application can be run from the project root by running:
```bash
dotnet watch run --project Server/blazor-flips.Server.csproj -- -c Debug
```

All things going to plan this should launch a browser instance with the application running in the browser. 

If the user clicks on the "run highs test" button on the index page the application will call out to the native library with a simple input vector created in the test case code.

If we inspect the output in the browser dev console there is a stack trace of an unhandled exception. This seems to be coming from the native libhighs.a library itself.

After stepping through wasm in the Chrome DevTools debugger it appears the library is not behaving correctly when built for wasm. After running the same code through FSX test on X86_64 with libhighs.dll as the target highs library which works ok I beleive there is something unusual going on with this code when built in the wasm context.

Note also that the application cannot be "published"; running the dotnet pulish command will result in the build failing with:

```code
...
error MSB4018: The "ManagedToNativeGenerator" task failed unexpectedly
...
error MSB4018: System.BadImageFormatException: This PE image is not a managed executa 
ble.
```
This however, appears to be due to the Optano library (dependency from Flips), but I've been unable to confirm this; removing the native file reference has not effect, but removing all references to Flips does.
