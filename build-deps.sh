#!/usr/bin/env bash

# adopted from https://github.com/lovasoa/highs-js

# Setup the Emscripten environment
pushd emsdk
source emsdk_env.sh
emsdk install 3.1.12
emsdk activate 3.1.12
popd

# Build HiGHS wasm library
set -e

root="$(dirname $(realpath "$0"))"

pushd or-tools/tools
export TARGET=wasm
export PROJECT=or-tools

# Cross compile a .NET solution of Google OR-Tools to webassembly
./cross_compile.sh

# Move artifacts into controlled location
cp ../build_cross/wasm/dotnet/packages/Google.OrTools.9.0.9050.nupkg ../../dependencies/or-tools/wasm
cp ../build_cross/wasm/lib/google-ortools-native.a ../../dependencies/or-tools/wasm

echo "Done"
popd